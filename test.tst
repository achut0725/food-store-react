import { useState } from "react";
import { RestaurantList } from "../../Configs/RestaurentList";
import RestaurantCard from "../RestaurantCard/RestaurantCard";

const BodyComponent = () => {
  
  const [searchText, setSearchText] = useState("");

  const filteredRestaurants = RestaurantList.filter((restaurant) => {
    return restaurant.data.name
      .toLowerCase()
      .includes(searchText.toLowerCase());
  });

  console.log(filteredRestaurants);

  return (
    <>
      <div className="container">
        <div className="bodyHead">
          <h1>Restaurants</h1>
          <div className="bodySearch">
            <input
              type="text"
              className="searchInput"
              placeholder=" Search Restaurants "
              value={searchText}
              onChange={(e) => {
                setSearchText(e.target.value);
              }}
            />
            <button className="searchButton">Search {searchText}</button>
          </div>
        </div>

        {!filteredRestaurants ? (
          <div className="restaurantList">
            {RestaurantList.map((restaurant) => {
              // using spread operator
              return (
                <RestaurantCard {...restaurant.data} key={restaurant.data.id} />
              );
            })}
          </div>
        ) : (
          <div className="restaurantList">
            {filteredRestaurants.map((restaurant) => {
              // using spread operator
              return (
                <RestaurantCard {...restaurant.data} key={restaurant.data.id} />
              );
            })}
          </div>
        )}
      </div>
    </>
  );
};

export default BodyComponent;

this if for the live changes in RestaurantList while i search.








search component ==> 


import React, { useState } from "react";
import { useFilterData } from "../../Utils/CustomHooks/FilterData";

const Search = ({ restaurants, palceholderText, setFilteredRestaurants }) => {
  
  const [searchText, setSearchText] = useState("");

  const handleSearch = () => {
    const filteredRestaurants = useFilterData(restaurants, searchText);   //imported filterData from the customHooks is using here
    setFilteredRestaurants(filteredRestaurants);
  };

  return (
    <div className="bodySearch">
      <input
        type="text"
        className="searchInput"
        placeholder={palceholderText}
        value={searchText}
        onChange={(e) => {
          setSearchText(e.target.value);
        }}
      />
      <button className="searchButton" onClick={handleSearch}>
        <img
          width="25"
          height="25"
          src="https://img.icons8.com/color/48/search--v1.png"
          alt="search--v1"
        />
      </button>
    </div>
  );
};

export default Search;

css ==> 
.bodySearch {
    display: flex;
    flex-direction: row;
    grid-gap: 10px;
    margin-top: 5px;
}

.searchInput {
    padding: 8px;
    width: 200px;
    border-radius: 50px;
    border: 2.5px solid rgb(182, 182, 182);
    box-shadow: 0px 3px 5px rgba(0,0,0,0.2);
}

.searchInput:hover {
    border: 2px solid gray;
}

.searchInput:focus {
    outline: none;
}

.searchButton {
    display: flex;
    justify-content: center;
    align-items: center;
    border: 2.5px solid rgb(182, 182, 182);
    background-color: rgb(214, 214, 214);
    width: 37px;
    border-radius: 0 50px 50px 0;
    margin-left: -40px;
}

.searchButton:hover {
    border: 2px solid gray;
    background-color: rgb(194, 194, 194);
}















import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addItem, removeItem } from "./src/Utils/Store/CartSlice";

function Test() {

  const dispatch = useDispatch();

  const items = [
    "banana",
    "cherry",
    "apple",
    "grapes",
    "orange",
    "strawberry",
    "tomato",
  ];

  const [array, setArray] = useState([]);

  const handleAddButtonClick = (item) => {

    dispatch(addItem(item));

    setArray((prevArray) => {
      const newArray = [...prevArray];
      let existingArrayIndex = -1;

      for (let i = 0; i < newArray.length; i++) {
        if (Array.isArray(newArray[i]) && newArray[i].includes(item)) {
          existingArrayIndex = i;
          break;
        }
      }

      if (existingArrayIndex !== -1) {
        newArray[existingArrayIndex].push(item);
      } else {
        newArray.push([item]);
      }

      return newArray;
    });
  };

  const handleRemoveButtonClick = (item) => {

    dispatch(removeItem(item));

    setArray((prevArray) => {
      const newArray = [...prevArray];
      for (let i = 0; i < newArray.length; i++) {
        if (Array.isArray(newArray[i]) && newArray[i].includes(item)) {
          const itemIndex = newArray[i].indexOf(item);
          newArray[i].splice(itemIndex, 1);

          // If the array becomes empty after removing the item, remove the entire array
          if (newArray[i].length === 0) {
            newArray.splice(i, 1);
          }
          break;
        }
      }
      return newArray;
    });
  };

  // console.log(array);

  const cartItems = useSelector((store) => store.cart.items);

  // console.log(cartItems);

  return (
    <>
      <div className="p-5">
        <h1>{array.length}</h1>

        {items.map((item, index) => {
          return (
            <div key={index} className="flex gap-4">
              <h2>{item}</h2>
              <button onClick={() => handleAddButtonClick(item)}>Add</button>
              <button onClick={() => handleRemoveButtonClick(item)}>
                Remove
              </button>
            </div>
          );
        })}
      </div>
    </>
  );
}

export default Test;
