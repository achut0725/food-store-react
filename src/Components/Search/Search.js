import React, { useState, useContext } from "react";
import { useFilterData } from "../../Utils/CustomHooks/FilterData";
// import UserContext from "../../Utils/Shared/UserContext";

const Search = ({ restaurants, palceholderText, setFilteredRestaurants }) => {
  // const {user, setUser} = useContext(UserContext)

  const [searchText, setSearchText] = useState("");

  const handleSearch = () => {
    const filteredRestaurants = useFilterData(restaurants, searchText); //imported filterData from the customHooks is using here
    setFilteredRestaurants(filteredRestaurants);
  };

  return (
    <div className="my-3 flex shadow-md rounded-full">
      <input
        className="p-1.5 border-solid border-2 border-gray-300 rounded-l-full outline-none shadow-inner"
        type="text"
        placeholder={" " + palceholderText}
        value={searchText}
        onChange={(e) => {
          setSearchText(e.target.value);
        }}
      />
      <button
        className="flex justify-center items-center border-solid border-2 border-s-transparent border-gray-300 rounded-r-full w-9 bg-slate-200"
        onClick={handleSearch}
      >
        <img
          width="25"
          height="25"
          src="https://img.icons8.com/color/48/search--v1.png"
          alt="search--v1"
        />
      </button>
      {/* 
      <input
        className="p-1.5 border-solid border-2 border-gray-300 rounded-full outline-none shadow-inner"
        type="text"
        // placeholder={" " + palceholderText}
        value={user.name}
        onChange={(e) => {
          setUser({
            ...user,
            name: e.target.value
          });
        }}
      /> */}
    </div>
  );
};

export default Search;
