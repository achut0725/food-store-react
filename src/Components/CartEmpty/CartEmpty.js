import React from "react";

function CartEmpty() {
  return (
    <>
      <div className="absolute z-10 w-72 -ml-52 p-4 border-2 border-orange-500 bg-white/90 rounded-xl shadow-xl">
        <h1 className="text-3xl font-bold">Cart Is Empty</h1>
        <h1>
          Good food is always cooking! <br />
          Go ahead, order some yummy <br />
          items from the menu.
        </h1>
        <h1 className="text-green-600">Add some items to see the Cart</h1>
      </div>
    </>
  );
}

export default CartEmpty;
