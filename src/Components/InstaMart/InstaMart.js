import React, { useContext, useEffect, useState } from "react";
import OrderCard from "../OrderCard/OrderCard";
import UserContext from "../../Utils/Shared/UserContext";
import LoginContext from "../../Utils/Shared/LoginContext";

function Instamart() {
  const [myOrders, setMyOrders] = useState([]);

  // console.log(myOrders)

  const { loggedinStatus, setLoggedInStatus } = useContext(LoginContext);

  const handleLogin = () => {
    setLoggedInStatus({
      status: true,
    });
  };

  const { user } = useContext(UserContext);

  useEffect(() => {
    const myOrdersFromLoaclStorage = [];

    const totalItems = localStorage.length;
    for (let i = 0; i < totalItems; i++) {
      const key = localStorage.key(i);
      const data = localStorage.getItem(key);
      const orders = JSON.parse(data);
      // console.log(orders);
      myOrdersFromLoaclStorage.push(orders);
    }

    setMyOrders(myOrdersFromLoaclStorage);
  }, []);

  return (
    <>
      {loggedinStatus.status ? (
        <div className="p-3 flex flex-col gap-4">
          <h1 className="text-xl font-bold">My Profile</h1>

          <div>
            <h1>{user.name}</h1>
            <h1>{user.email}</h1>
            <h1>{user.phone}</h1>
          </div>

          <p className="text-lg font-semibold">My Orders</p>
          <div className="flex flex-row gap-3">
            {myOrders &&
              myOrders.map((order) => {
                return <OrderCard key={order.totalPrice} order={order} />;
              })}
          </div>
        </div>
      ) : (
        <div className="p-2 my-10 flex flex-col items-center gap-4 h-80">
          <div>
            <h1 className="text-lg font-semibold">Account</h1>
            <h1>Please Login or Sign Up</h1>
          </div>

          <div className="w-auto flex gap-5">
            <button
              onClick={() => handleLogin()}
              className="px-2 py-1 border-2 border-green-600 hover:bg-green-400/30 rounded-lg"
            >
              Login
            </button>

            <button className="border-2 border-green-600 hover:bg-green-400/30 px-2 py-1 rounded-lg">
              Sign Up
            </button>
          </div>
        </div>
      )}
    </>
  );
}

export default Instamart;
