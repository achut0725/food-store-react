import React from "react";

function RestaurantPageShimmer() {
  const mockData = 5;

  return (
    <>
      <div className="p-4 flex-col">
        <div className="flex flex-row gap-16 justify-center mb-5">
          <div className="bg-gray-400 w-96 h-52 rounded-xl"></div>
          <div className="flex flex-col gap-4">
            <div className="bg-gray-400 w-72 h-5"></div>
            <div className="bg-gray-400 w-52 h-4"></div>
            <div className="bg-gray-400 w-52 h-3"></div>
            <div className="bg-gray-400 w-28 h-3"></div>
          </div>
        </div>

        <div className="flex flex-col">
          <div className="bg-gray-400 w-72 h-5 ml-64 my-4"></div>

          <div className="foodInfo flex gap-4 justify-center">
            <div className="flex flex-col gap-4 ">
              {Array.from({ length: mockData }, (_, index) => (
                <div
                  key={index}
                  className="flex flex-row bg-gray-300 p-4 w-[800px] h-52 border-t-2 border-gray-300 gap-4 justify-between rounded-xl"
                >
                  <div className="flex flex-col gap-4">
                    <div className="bg-gray-400 w-64 h-5"></div>
                    <div className="bg-gray-400 w-52 h-4"></div>
                    <div className="bg-gray-400 w-80 h-4"></div>
                  </div>

                  <div className="bg-gray-400 w-52 h-36 rounded-xl"></div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default RestaurantPageShimmer;
