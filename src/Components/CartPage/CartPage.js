import React, { useContext, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import store from "../../Utils/Store/Store";
import { clearCart } from "../../Utils/Store/CartSlice";
import CartItemCard from "../CartItemCard/CartItemCard.tsx";
import { Link } from "react-router-dom";
import LoginContext from "../../Utils/Shared/LoginContext";
import OrderPopup from "../OrderPopup/OrderPopup";

function CartPage() {
  const cartItems = useSelector((store) => store.cart.items); //*this line means like ==> stands for "i'm subscribing the items from cart from store"
  // console.log(cartItems);

  const dispatch = useDispatch();

  const [showPop, setShowPop] = useState(false);

  const [addrssTosStore, setAddressTosStore] = useState("");
  // console.log(addrssTosStore);

  const [error, setError] = useState(null);

  const handleClearCart = () => {
    dispatch(clearCart());
    setAddressTosStore("");
    setError(null);
  };

  const sum = cartItems.reduce((acc, subArray) => {
    const subArrayTotal = subArray.reduce(
      (subtotal, item) => subtotal + (item.price || item.defaultPrice),
      0
    );
    return acc + subArrayTotal;
  }, 0);

  const totalPrice = sum / 100;
  // console.log(totalPrice);

  const { loggedinStatus, setLoggedInStatus } = useContext(LoginContext);

  const handleLogin = () => {
    setLoggedInStatus({
      status: true,
    });
  };

  const [address, setAddress] = useState("");

  const handleStoreAdress = () => {
    if (address.length === 0) {
      setError("Please enter the address");
    } else if (cartItems.length === 0) {
      setError("Cart is empty");
    } else {
      setAddressTosStore(address);
      setError(null);
    }
  };

  const [payment, setPayment] = useState("Payment pending");
  // console.log(payment);

  const [paymentStatus, setPaymentStatus] = useState(false);
  // console.log(paymentStatus);

  const [itemsToStore, setItemsToStore] = useState({
    items: cartItems,
    address: address,
    paymentStatus: paymentStatus,
    paymentType: payment,
    totalPrice: totalPrice,
  });

  console.log(itemsToStore);

  const timestamp = Date.now();
  const key = `orderedItemsInStore_${timestamp}`;

  useEffect(() => {
    if (itemsToStore.address.length > 0) {
      window.localStorage.setItem(key, JSON.stringify(itemsToStore));
    }
  }, [payment]);

  const handlePaynow = () => {
    if (addrssTosStore.length === 0) {
      setError("Please enter the address");
    } else {
      setPaymentStatus(true);
      setPayment("Online payment");
      setError(null);
      setItemsToStore({
        ...itemsToStore,
        address: addrssTosStore,
        paymentStatus: true,
        paymentType: "Online payment",
        totalPrice: totalPrice,
      });
      setShowPop(true);
      setTimeout(() => {
        setShowPop(false);
      }, 3000);
    }
  };

  const handleCashOnDelivey = () => {
    if (addrssTosStore.length === 0) {
      setError("Please enter the address");
    } else {
      setPayment("Cash on delivery");
      setPaymentStatus(true);
      setError(null);
      setItemsToStore({
        ...itemsToStore,
        address: addrssTosStore,
        paymentStatus: true,
        paymentType: "Cash on delivery",
        totalPrice: totalPrice,
      });
      setShowPop(true);
      setTimeout(() => {
        setShowPop(false);
      }, 3000);
    }
  };

  const inputClassName = `p-1.5 border-solid border-2 rounded-lg outline-none shadow-inner ${
    error ? "border-red-500 placeholder-red-700" : "border-gray-400"
  }`;

  return (
    <>
      <div className="p-3 flex flex-row justify-center gap-4">
        <div className="w-auto h-96 flex flex-col gap-4 p-3 border-2 border-gray-500 shadow-lg ">
          {loggedinStatus.status === false ? (
            <div className="bg-white w-96 p-2 flex flex-col gap-4 border-2 border-gray-500 shadow-lg">
              <div>
                <h1 className="text-lg font-semibold">Account</h1>
                <h1>
                  To place your order now, log in to your existing account or
                  sign up.
                </h1>
              </div>

              <div className="w-auto flex gap-5">
                <button
                  onClick={() => handleLogin()}
                  className="px-2 py-1 border-2 border-green-600 hover:bg-green-400/30 rounded-lg"
                >
                  Login
                </button>

                <button className="border-2 border-green-600 hover:bg-green-400/30 px-2 py-1 rounded-lg">
                  Sign Up
                </button>
              </div>
            </div>
          ) : (
            <div className="flex flex-col gap-4">
              <div className="bg-white w-96 p-2 flex flex-col gap-4 border-2 border-gray-500 shadow-lg">
                <div>
                  <div className="flex gap-2">
                    <h1 className="text-lg font-semibold text-green-600">
                      Account Verified
                    </h1>
                    <img
                      width="25"
                      height="25"
                      src="https://img.icons8.com/color/48/checked-2--v1.png"
                      alt="checked-2--v1"
                    />
                  </div>
                  <h1>Check your address and palce the order</h1>
                </div>
              </div>

              <div className="bg-white w-96 p-2 flex flex-col gap-4 border-2 border-gray-500 shadow-lg">
                <h1>Enter your adrress</h1>

                <div className="flex items-center gap-2">
                  <div className="flex flex-col gap-1">
                    <input
                      className={inputClassName}
                      type="text"
                      required
                      placeholder={error ? error : "Enter your address"}
                      value={address}
                      onChange={(e) => setAddress(e.target.value)}
                    />
                    {error && (
                      <p className="text-sm text-red-500 font-semibold">
                        {error}
                      </p>
                    )}
                  </div>

                  {addrssTosStore ? (
                    <div className="flex gap-1">
                      <h1 className="text-lg font-semibold text-green-600">
                        Submitted
                      </h1>
                      <img
                        width="25"
                        height="27"
                        src="https://img.icons8.com/color/48/checked-2--v1.png"
                        alt="checked-2--v1"
                      />
                    </div>
                  ) : (
                    <button
                      className="hover:bg-blue-400/30 border-2 border-blue-400 p-1 rounded-lg"
                      onClick={() => handleStoreAdress()}
                    >
                      Submit
                    </button>
                  )}
                </div>
              </div>

              {cartItems.length > 0 ? (
                <div className="bg-white w-96 p-2 flex flex-col gap-2 border-2 border-gray-500 shadow-lg">
                  <h1>Payments</h1>

                  <div className="flex flex-col gap-2">
                    <h1 className="text-lg font-bold">₹ {totalPrice}/-</h1>
                    {!paymentStatus ? (
                      <div className="flex gap-4">
                        <button
                          className="hover:bg-blue-400/30 border-2 border-blue-400 p-1 rounded-lg"
                          onClick={() => handlePaynow()}
                        >
                          Pay Now
                        </button>
                        <button
                          className="hover:bg-blue-400/30 border-2 border-blue-400 p-1 rounded-lg"
                          onClick={() => handleCashOnDelivey()}
                        >
                          Cash on delivery
                        </button>
                      </div>
                    ) : (
                      <div className="flex gap-2">
                        <h1 className="text-lg font-semibold text-green-600">
                          {payment}
                        </h1>
                        <img
                          width="25"
                          height="25"
                          src="https://img.icons8.com/color/48/checked-2--v1.png"
                          alt="checked-2--v1"
                        />
                      </div>
                    )}
                  </div>
                </div>
              ) : (
                <div className="bg-white w-96 p-2 flex flex-col gap-2 border-2 border-gray-500 shadow-lg">
                  <h1>Cart is empty</h1>
                </div>
              )}
            </div>
          )}
        </div>

        <div className="flex flex-col gap-4 border-2 border-gray-500 shadow-lg p-2">
          {cartItems &&
            cartItems.map((cartItem) => {
              return <CartItemCard key={cartItem[0].id} cartItem={cartItem} />;
            })}
          <div>
            {cartItems.length > 0 && (
              <button
                className="p-1 border-2 border-red-500 rounded-lg mb-2 hover:bg-orange-400/30"
                onClick={() => handleClearCart()}
              >
                Clear Cart
              </button>
            )}

            {cartItems.length > 0 ? (
              <div className="flex flex-row justify-between p-4 border-t-2 border-gray-500">
                <h1 className="text-lg font-semibold">
                  Total Price Payable -{" "}
                </h1>
                <h1 className="text-lg font-bold">₹ {totalPrice}/-</h1>
              </div>
            ) : (
              <div className="p-4 flex flex-col gap-4 place-content-center ">
                <h1 className="text-3xl font-bold">Your cart is empty</h1>

                <h1 className="">
                  You can go to home page to view more restaurants
                </h1>

                <Link to={"/"}>
                  <h1 className="text-xl font-bold bg-orange-500 rounded-lg p-3 flex justify-center shadow-lg hover:text-white">
                    See Restaurants Near You
                  </h1>
                </Link>
              </div>
            )}
          </div>
        </div>
      </div>

      {showPop && <OrderPopup />}
    </>
  );
}

export default CartPage;
