import React from "react";

function BodyShimmerUi() {
  return (
    <>
      <div className="container p-3 w-60 h-72 rounded-lg shadow-2xl flex flex-col gap-3">
        <div className="card h-36 rounded-t-lg bg-gray-400"></div>
        <div className="h1 bg-gray-400 w-44 h-4"></div>
        <div className="h2 bg-gray-400 w-40 h-4"></div>
      </div>
    </>
  );
}

export default BodyShimmerUi;
