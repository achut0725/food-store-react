import { useContext } from "react";
import { Title } from "../Header/HeaderComponent";
import UserContext from "../../Utils/Shared/UserContext";
import { Link } from "react-router-dom";

const FooterComponent = () => {
  const { user } = useContext(UserContext);

  return (
    <footer className="p-5 flex flex-col gap-3 justify-center items-center border-solid border-2 border-t-gray-500">
      <Title />
      <h2>This website is developed by {user.name}</h2>
      <Link to={"/test"}>Test</Link>
    </footer>
  );
};

export default FooterComponent;
