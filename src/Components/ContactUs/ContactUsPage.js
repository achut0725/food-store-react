import React from "react";

const ContactUsPage = () => {
  return (
    <div className="p-3 flex gap-10">
      <div className="contact-info flex flex-col gap-3">
        <div>
          <h1>Contact Us</h1>
          <p>Have a question or feedback? Get in touch with us!</p>
        </div>
        <h2>Contact Information</h2>
        <p>Phone: 123-456-7890</p>
        <p>Email: info@foodstore.com</p>
        <p>Address: 123 Main Street, City, State</p>
      </div>

      <div className="contact-form ">
        <h2>Send us a Message</h2>
        <form className="flex flex-col w-80 gap-2">
          <label htmlFor="name">Name:</label>
          <input
            type="text"
            id="name"
            name="name"
            required
            className="p-1.5 border-solid border-2 rounded-lg outline-none shadow-inner border-gray-400"
          />

          <label htmlFor="email">Email:</label>
          <input
            type="email"
            id="email"
            name="email"
            required
            className="p-1.5 border-solid border-2 rounded-lg outline-none shadow-inner border-gray-400"
          />

          <label htmlFor="message">Message:</label>
          <textarea
            id="message"
            name="message"
            rows="5"
            required
            className="p-1.5 border-solid border-2 rounded-lg outline-none shadow-inner border-gray-400"
          ></textarea>

          <button
            type="submit"
            className="hover:bg-blue-400/30 border-2 border-blue-400 p-1 rounded-lg w-36"
          >
            Send
          </button>
        </form>
      </div>
    </div>
  );
};

export default ContactUsPage;
