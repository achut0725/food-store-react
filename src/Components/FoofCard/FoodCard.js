import React, { useState, useEffect } from "react";
import { IMG_CDN_URL } from "../../Configs/ImageConfig";
import { addItem, removeItem } from "../../Utils/Store/CartSlice";
import { useDispatch, useSelector } from "react-redux";
import store from "../../Utils/Store/Store";

function FoodCard(foodInfo) {
  let foodInfoArray = [];

  const foodInfoToDisplayInCard = foodInfo?.foodInfo?.card?.info;

  foodInfoArray.push(foodInfoToDisplayInCard);
  // console.log(foodInfoArray);

  const cartItems = useSelector((store) => store.cart.items);
  // console.log(cartItems)

  const [count, setCount] = useState({});
  // console.log(count)

  useEffect(() => {
    const selectedCount = {};

    cartItems.forEach((subArray) => {
      subArray.forEach((element) => {
        const id = element.id;
        selectedCount[id] = (selectedCount[id] || 0) + 1;
      });
    });

    const element = foodInfoArray[0];
    const id = element.id;
    const name = element.name;
    const itemCount = selectedCount[id] || 0;

    setCount(itemCount);
  }, [cartItems]);

  const convertedPrice =
    (foodInfoToDisplayInCard?.defaultPrice || foodInfoToDisplayInCard?.price) /
    100;

  const dispatch = useDispatch();

  const handleAddItem = () => {
    dispatch(addItem(foodInfoToDisplayInCard));
  };

  const handleRemoveItem = () => {
    dispatch(removeItem(foodInfoToDisplayInCard));
  };

  return (
    <>
      <div className="foodCardContainer flex p-4 w-[800px] border-t-2 border-gray-300 gap-4 justify-between rounded-xl hover:shadow-xl">
        <div className="foodCardContainersection1 w-[530px]">
          <div className="foodCardContainersection1sub">
            {foodInfoToDisplayInCard?.itemAttribute?.vegClassifier ===
            "NONVEG" ? (
              <img
                width="30"
                height="30"
                src="https://img.icons8.com/color/48/non-vegetarian-food-symbol.png"
                alt="non-vegetarian-food-symbol"
              />
            ) : (
              <img
                width="30"
                height="30"
                src="https://img.icons8.com/color/48/vegetarian-food-symbol.png"
                alt="vegetarian-food-symbol"
              />
            )}
            <h3 className="text-xl font-semibold">
              {foodInfoToDisplayInCard?.name}
            </h3>
            {convertedPrice && (
              <h4 className="text-lg font-semibold">₹ {convertedPrice}/-</h4>
            )}
          </div>
          <p>{foodInfoToDisplayInCard?.description}</p>
        </div>

        <div className="foodCardContainersection2">
          {foodInfoToDisplayInCard?.imageId ? (
            <img
              src={IMG_CDN_URL + foodInfoToDisplayInCard?.imageId}
              alt="image"
              className="foodImage w-52 rounded-xl"
            />
          ) : (
            <div className="foodSection2 bg-gray-400 w-52 h-32 rounded-xl">
              No image
            </div>
          )}

          {count === 0 ? (
            <div className="flex flex-row justify-between bg-white w-28 h-5 ms-12 rounded-lg shadow-indigo-500/40 z-10 cursor-pointer">
              <div
                className="flex place-content-center bg-white p-1 w-32 -mt-6 rounded-lg shadow-md shadow-black cursor-pointer"
                onClick={() => handleAddItem()}
              >
                <h1 className="m-1.5 text-green-600 cursor-pointer">Add</h1>
              </div>
            </div>
          ) : (
            <div className="flex flex-row justify-between bg-white w-28 ms-12 h-5 rounded-lg shadow-indigo-500/40 ">
              <div className="flex flex-row justify-between bg-white p-1 w-32 -mt-6  rounded-lg shadow-md shadow-black">
                <button
                  className="px-2 text-2xl rounded-md text-green-700 hover:bg-green-200 cursor-pointer"
                  onClick={() => handleAddItem()}
                >
                  <img
                    width="23"
                    height="23"
                    src="https://img.icons8.com/color/48/plus--v1.png"
                    alt="plus--v1"
                  />
                </button>

                {count > 0 && <h1 className="mt-2">{count}</h1>}

                <button
                  className="px-2 text-2xl rounded-md text-orange-700 hover:bg-orange-200 cursor-pointer"
                  onClick={() => handleRemoveItem()}
                >
                  <img
                    width="23"
                    height="23"
                    src="https://img.icons8.com/color/48/minus.png"
                    alt="minus"
                  />
                </button>
              </div>
            </div>
          )}
        </div>
      </div>
    </>
  );
}

export default FoodCard;
