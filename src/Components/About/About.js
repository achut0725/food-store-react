import { Link, Outlet } from "react-router-dom";
import React from "react";
import Section from "./Section";
import UserContext from "../../Utils/Shared/UserContext";

class About extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};

    // console.log("parent - constructor")  //*1
  }

  componentDidMount() {
    // console.log("parent - componentDidMount")   //*2
  }

  render() {
    // console.log("parent - render")   //*3

    return (
      <>
        <div className="p-4">
          <h1>About</h1>
          <p>
            Food Store is a convenient and user-friendly food ordering website
            that caters to your culinary cravings. With a vast selection of
            cuisines and restaurants to choose from, Food Store offers a
            seamless browsing and ordering experience. From tasty appetizers to
            delightful desserts, satisfy your taste buds with just a few clicks
            at Food Store.
          </p>

          <UserContext.Consumer>
            {(value) => <h3 className="font-bold">{value.user.name}</h3>}
          </UserContext.Consumer>

          <Link to={"profile"}>Profile</Link>

          <Outlet />
        </div>
      </>
    );
  }
}

export default About;
