type CartItemCardProp = {
  cartItem: {
    name: string;
    price: number;
    defaultPrice: number;
  }[];
};

import React from "react";
import { useDispatch } from "react-redux";
import { addItem, removeItem } from "../../Utils/Store/CartSlice";

function CartItemCard({ cartItem }: CartItemCardProp) {
  const totalPrice = cartItem.reduce(
    (acc, item) => acc + (item.price || item.defaultPrice),
    0
  );
  // console.log(totalPrice);
  const itemsTotalPrice = (totalPrice / 100).toFixed(2);
  // console.log(itemsTotalPrice);

  const dispatch = useDispatch();

  const handleAddItem = () => {
    dispatch(addItem(cartItem[0]));
  };

  const handleRemoveItem = () => {
    dispatch(removeItem(cartItem[0]));
  };

  return (
    <>
      <div className="flex gap-5 justify-between p-2 align-middle">
        <div className="flex gap-4 justify-between">
          <div className="flex-col gap-4">
            <h1 className="w-64 text-md font-semibold">{cartItem[0].name}</h1>
            <h1 className="">
              ₹ {(cartItem[0].price || cartItem[0].defaultPrice) / 100}/- Each
            </h1>
          </div>
          <div className="flex flex-row justify-between bg-white p-1 w-28 rounded-md shadow-md shadow-black">
            <button
              className="p-1 text-2xl rounded-md text-green-700 hover:bg-green-200 cursor-pointer"
              onClick={() => handleAddItem()}
            >
              <img
                width="23"
                height="23"
                src="https://img.icons8.com/color/48/plus--v1.png"
                alt="plus--v1"
              />
            </button>

            {cartItem && <h1 className="mt-2">{cartItem.length}</h1>}

            <button
              className="p-1 text-2xl rounded-md text-orange-700 hover:bg-orange-200 cursor-pointer"
              onClick={() => handleRemoveItem()}
            >
              <img
                width="23"
                height="23"
                src="https://img.icons8.com/color/48/minus.png"
                alt="minus"
              />
            </button>
          </div>
        </div>
        <h1 className="text-md font-bold">₹ {itemsTotalPrice}/-</h1>
      </div>
    </>
  );
}

export default CartItemCard;
