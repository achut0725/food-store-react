import { useContext, useState } from "react";
import { Link } from "react-router-dom";
import { useOnline } from "../../Utils/CustomHooks/checkOnline";
import UserContext from "../../Utils/Shared/UserContext";
import { useSelector } from "react-redux";
import store from "../../Utils/Store/Store";
import LoginContext from "../../Utils/Shared/LoginContext";
import CartEmpty from "../CartEmpty/CartEmpty";

export const Title = () => (
  <>
    <Link to="/" className="homeLink">
      <div className="flex space-x-3 items-center">
        <img
          width="50"
          height="50"
          src="https://img.icons8.com/arcade/64/fast-food.png"
          alt="food store"
        />
        <h1 className="text-3xl font-bold">Food store</h1>
      </div>
    </Link>
  </>
);

const HeaderComponent = () => {
  const { loggedinStatus, setLoggedInStatus } = useContext(LoginContext);
  // console.log(loggedinStatus.status)

  const isOnline = useOnline();

  const { user } = useContext(UserContext);

  const cartItems = useSelector((store) => store.cart.items);
  // console.log(cartItems);

  const totalNumberOfItems = cartItems.reduce((count, innerArray) => {
    return count + innerArray.length;
  }, 0);
  // console.log(totalNumberOfItems)

  const handleLogin = () => {
    setLoggedInStatus({
      status: true,
    });
  };

  const handleLogout = () => {
    setLoggedInStatus({
      status: false,
    });
  };

  const [showContent, setShowContent] = useState(false);

  const handleMouseEnter = () => {
    setShowContent(true);
  };

  const handleMouseLeave = () => {
    setShowContent(false);
  };

  return (
    <header className="flex justify-between items-center p-3 shadow-lg shadow-black-500/50">
      <Title />

      <div className="navItems">
        <ul className="flex space-x-5">
          {isOnline ? (
            <img
              width="24"
              height="24"
              src="https://img.icons8.com/material-outlined/24/online--v1.png"
              alt="online--v1"
            />
          ) : (
            <img
              width="24"
              height="24"
              src="https://img.icons8.com/material/24/offline--v1.png"
              alt="offline--v1"
            />
          )}
          {loggedinStatus.status && <p>{user.name}</p>}
          <li>
            <Link to={"/"}>Home</Link>
          </li>
          <li>
            <Link to={"/about"}>About</Link>
          </li>

          <li>
            <Link to={"/contactUs"}>Contact Us</Link>
          </li>

          <li>
            <Link to={"/instaMart"}>Profile</Link>
          </li>

          <li>
            {cartItems.length > 0 ? (
              <Link
                to={"/cartPage"}
                className="flex flex-row gap-1 cursor-pointer"
              >
                {" "}
                <h3>Cart</h3>
                {cartItems.length > 0 && <h3>- {totalNumberOfItems}</h3>}
              </Link>
            ) : (
              <div>
                <h3
                  className="cursor-pointer"
                  onMouseEnter={handleMouseEnter}
                  onMouseLeave={handleMouseLeave}
                >
                  Cart
                </h3>

                {showContent && <CartEmpty />}
              </div>
            )}
          </li>

          {loggedinStatus.status ? (
            <button onClick={() => handleLogout()} className="loginButton">
              Logout
            </button>
          ) : (
            <button onClick={() => handleLogin()} className="loginButton">
              Login
            </button>
          )}
        </ul>
      </div>
    </header>
  );
};

export default HeaderComponent;
