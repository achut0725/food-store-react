import { useEffect, useState } from "react";
import { RestaurantListHC } from "../../Configs/RestaurentList";
import RestaurantCard from "../RestaurantCard/RestaurantCard";
import BodyShimmerUi from "../BodyShimmerUi/BodyShimmerUi";
import { Link } from "react-router-dom";
import SearchRestaurants from "../Search/Search";
import { useGetData } from "../../Utils/CustomHooks/FetchData";
import { useOnline } from "../../Utils/CustomHooks/checkOnline";

const BodyComponent = () => {
  const mokeData = 10;

  // const fetchDataURL = "https://www.swiggy.com/dapi/restaurants/list/v5?lat=9.9312328&lng=76.26730409999999&page_type=DESKTOP_WEB_LISTING";

  // const restaurantsFromGetData = useGetData(fetchDataURL)

  // const restaurants2 = restaurantsFromGetData?.data?.cards[2]?.data?.data?.cards;
  // console.log(restaurants2)

  // const fileteredRestaurants2 = restaurantsFromGetData?.data?.cards[2]?.data?.data?.cards;
  // console.log(fileteredRestaurants2)

  const [restaurants, setRestaurants] = useState([]);

  const [fileteredRestaurants, setFileteredRestaurants] = useState([]);

  useEffect(() => {
    getRestaurants();
  }, []);

  async function getRestaurants() {
    const response = await fetch(
      "https://www.swiggy.com/dapi/restaurants/list/v5?lat=9.9312328&lng=76.26730409999999&page_type=DESKTOP_WEB_LISTING"
    );
    const data = await response.json();
    // console.log(data);
    const RestaurantList = data?.data?.cards[2]?.data?.data?.cards;
    setRestaurants(RestaurantList);
    setFileteredRestaurants(RestaurantList);
  }

  const onLine = useOnline();

  if (!onLine) {
    return (
      <>
        <div className="offline">
          <h1>Oops !!</h1>
          <h2>Something wend wrong</h2>
          <img
            width="100"
            height="100"
            src="https://img.icons8.com/stickers/100/wifi-off.png"
            alt="wifi-off"
          />
          <h3>Please check your internet connection</h3>
        </div>
      </>
    );
  }

  return (
    <>
      <div className="p-5">
        <div className="flex space-x-4 items-center mb-4">
          <h1 className="text-3xl font-semibold">Restaurants</h1>
          <SearchRestaurants
            restaurants={restaurants}
            setFilteredRestaurants={setFileteredRestaurants}
            palceholderText={"Search Restaurants"}
          />
        </div>

        {restaurants.length === 0 ? (
          <div className="restaurantList flex flex-wrap gap-5 place-content-center">
            {Array.from({ length: mokeData }, (_, index) => (
              <BodyShimmerUi key={index} />
            ))}
          </div>
        ) : (
          <div>
            {fileteredRestaurants.length === 0 ? (
              <h3>No restaurants fount !!</h3>
            ) : (
              <div className="flex flex-wrap gap-5 place-content-center">
                {fileteredRestaurants.map((restaurant) => {
                  return (
                    <Link
                      to={"/RestaurantPage/" + restaurant.data.id}
                      key={restaurant.data.id}
                    >
                      <RestaurantCard
                        {...restaurant.data}
                        key={restaurant.data.id}
                      />
                    </Link>
                  );
                })}
              </div>
            )}
          </div>
        )}
      </div>
    </>
  );
};

export default BodyComponent;
