import React, { useState, useEffect } from "react";
import Section from "../About/Section";
import ClassExample from "../ClassExample/ClassExample";

function Profile() {
  const [visibleSection, setVisibleSection] = useState("");

  useEffect(() => {}, []);

  return (
    <div>
      <h1>this is from the fundtional component</h1>

      <div className="flex-col p-2 space-y-5 ">
        <Section
          h1={"Section 1"}
          isVisible={visibleSection === "Section 1"}
          setIsVisible={() => setVisibleSection("Section 1")}
          setIsVisibleHide={() => setVisibleSection("")}
        />
        <Section
          h1={"Section 2"}
          isVisible={visibleSection === "Section 2"}
          setIsVisible={() => setVisibleSection("Section 2")}
          setIsVisibleHide={() => setVisibleSection("")}
        />
        <Section
          h1={"Section 3"}
          isVisible={visibleSection === "Section 3"}
          setIsVisible={() => setVisibleSection("Section 3")}
          setIsVisibleHide={() => setVisibleSection("")}
        />
      </div>
    </div>
  );
}

export default Profile;
