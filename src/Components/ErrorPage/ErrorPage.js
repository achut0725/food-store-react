import { useRouteError } from "react-router-dom";

import React from "react";

function ErrorPage() {
  
  const error = useRouteError();
  console.log(error);

  return (
    <>
      <div className="errorPage">
        <h1>Oops !!</h1>
        <h2>Something went wrong !!</h2>
        <img
          width="110"
          height="110"
          src="https://img.icons8.com/bubbles/100/error.png"
          alt="error"
        />
        <h3>{error?.status + " : " + error?.statusText}</h3>
        <h3>{error?.error?.message}</h3>
      </div>
    </>
  );
}

export default ErrorPage;
