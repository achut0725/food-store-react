import React from "react";
import ItemSection from "./ItemSection";

function OrderCard({ order }) {
  const itemsToMap = order.items;
  // console.log(itemsToMap)

  return (
    <>
      <div className="p-2 border-2 border-gray-500 shadow-lg flex flex-col gap-3">
        <div className="flex flex-col gap-3">
          {itemsToMap &&
            itemsToMap.map((item) => {
              return <ItemSection key={item[0].id} item={item} />;
            })}
        </div>
        <h1 className="">Address : {order.address}</h1>
        Payment Status :{" "}
        {order.paymentType === "Online payment" ? (
          <h1 className="text-green-600">Payment done</h1>
        ) : (
          <h1 className="text-yellow-600">Payment pending</h1>
        )}
        <h1>Payment mode : {order.paymentType}</h1>
        <h1>Total : ₹ {order.totalPrice}/-</h1>
      </div>
    </>
  );
}

export default OrderCard;
