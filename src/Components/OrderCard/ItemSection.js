import React from "react";

function ItemSection({ item }) {
  //   console.log(item);

  const amount = (item[0].price || item[0].defaultPrice) / 100;

  return (
    <>
      <div className="p-2 border-2 border-gray-500 shadow-lg">
        <div className="flex gap-2">
          <h1>{item[0].name}</h1>
          <h1>({item.length})</h1>
        </div>
        <h1>₹ {amount}/- Each</h1>
      </div>
    </>
  );
}

export default ItemSection;
