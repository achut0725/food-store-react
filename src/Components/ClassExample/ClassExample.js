import React from "react";
import UserContext from "../../Utils/Shared/UserContext";

class ClassExample extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      count: this.props.count,
      count2: 0,
      clickCount: 0,
      userInfo: {
        title: "",
        userId: "",
      },
    };

    // console.log("initial count" +this.state.count);   // do not chage
    // console.log("initial count2" +this.state.count2);

    this.handleButtonClick = () => {
      this.setState((prevState) => ({
        count2: prevState.count2 + this.props.count,
        clickCount: prevState.clickCount + 1, // Increment the click count
      }));

      // console.log(this.state.count);
      // console.log(this.state.count2);
      // console.log(this.state.clickCount);
    };

    // console.log(this.props.name + "child - constructor" )   //*1
  }

  async componentDidMount() {
    // console.log("child - componentDidMount");  //*2

    // const data = await fetch("https://jsonplaceholder.typicode.com/todos/1");
    // const json = await data.json();
    // this.setState({ userInfo: json });

    // console.log(json);

    // this.interval = setInterval(() => {             // here we are creating the interval   -- //!this is inside the class based component
    //   console.log("hello from class component")
    // }, 1000);
  }


  componentDidUpdate() {
    // console.log("child - componentDidUpdate");   //*3
  }

  componentWillUnmount() {
    // console.log("child - componentWillUnmount");   //*4
    // clearInterval(this.interval);                  // here we are clearing the interval  - this is necessary -
                                                      //  because the component has been removed from the DOM
  }                                                      // if we do not remove this the intervel will still work and the power
                                                            // increase as much as we come back to the component
  render() {

    const { count } = this.state;    //this place can use for destucturing, console.log, ....
    const { count2 } = this.state;    // this place can check weather it is rendered or not
    const { clickCount } = this.state;

    // console.log(this.props.name + "child - render")     //*5

    return (
      <div>
        <h1>Class Example</h1>
        <h1>Name : {this.props.name}</h1>

        <h1>{count}</h1>

        {/* <h1>{this.state.count2}</h1> */}     // instead of this

        <h1>{count2}</h1>     // in here i used the destructured data from this.state

        <p>Button Clicks: {clickCount}</p>{" "}

        {/* Display the click count */}
        <button onClick={this.handleButtonClick}>Increment</button>

        <div>
        <h3>{this?.state?.userInfo?.title}</h3>
        <h3>{this?.state?.userInfo?.userId}</h3>

        <UserContext.Consumer>                   //* in this way how we can use the useContext in class based components
          {(value)=> console.log(value)}
        </UserContext.Consumer>

        <UserContext.Consumer>                   //* in this way how we can use the useContext in class based components
          {(user)=> <div>
            <h1>{user.name}</h1>
             </div>}
        </UserContext.Consumer>

      </div>

      </div>
    );
  }
}

export default ClassExample;
