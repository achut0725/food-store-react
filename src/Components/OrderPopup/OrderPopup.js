import React from "react";

function OrderPopup() {
  return (
    <>
      <div className="overlay fixed top-0 left-0 w-full h-full flex justify-center items-center bg-black bg-opacity-50 z-40">
        <div className="popup relative w-60 p-5 bg-white rounded-lg shadow-md text-center border-2 border-orange-500 bg-white/90">
          <h1>Order Placed</h1>
        </div>
      </div>
    </>
  );
}

export default OrderPopup;
