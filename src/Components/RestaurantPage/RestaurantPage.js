import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { IMG_CDN_URL } from "../../Configs/ImageConfig";
import FoodCard from "../FoofCard/FoodCard";
import RestaurantPageShimmer from "../RestaurantPageShimmer/RestaurantPageShimmer";
import { useGetDataWithId } from "../../Utils/CustomHooks/FetchData";
import { FETCH_RESTAURANTINFO_URL } from "../../Configs/FetchData";
import { useSelector } from "react-redux";
import store from "../../Utils/Store/Store";

function RestaurantPage() {
  const { id } = useParams();

  const cartItems = useSelector((store) => store.cart.items);

  const restaurantInfo = useGetDataWithId(FETCH_RESTAURANTINFO_URL, id);

  const restaurantInfoToDisplay =
    restaurantInfo?.data?.cards[0]?.card?.card?.info;

  const title1 =
    restaurantInfo?.data?.cards[2]?.groupedCard?.cardGroupMap?.REGULAR?.cards[1]
      ?.card?.card?.title;

  const foodInfoToDisplay1 =
    restaurantInfo?.data?.cards[2]?.groupedCard?.cardGroupMap?.REGULAR?.cards[1]
      ?.card?.card?.itemCards;
  // console.log(foodInfoToDisplay1);

  const title2 =
    restaurantInfo?.data?.cards[2]?.groupedCard?.cardGroupMap?.REGULAR?.cards[2]
      ?.card?.card?.title;

  const foodInfoToDisplay2 =
    restaurantInfo?.data?.cards[2]?.groupedCard?.cardGroupMap?.REGULAR?.cards[2]
      ?.card?.card?.itemCards;
  // console.log(foodInfoToDisplay2);

  const title3 =
    restaurantInfo?.data?.cards[2]?.groupedCard?.cardGroupMap?.REGULAR?.cards[3]
      ?.card?.card?.title;

  const foodInfoToDisplay3 =
    restaurantInfo?.data?.cards[2]?.groupedCard?.cardGroupMap?.REGULAR?.cards[3]
      ?.card?.card?.itemCards;
  // console.log(foodInfoToDisplay3);

  return (
    <>
      {!restaurantInfoToDisplay ? (
        <RestaurantPageShimmer />
      ) : (
        <div className="p-4 flex-col">
          {restaurantInfoToDisplay && (
            <div className="restaurantInfo flex flex-row gap-16 justify-center mb-5">
              <img
                src={IMG_CDN_URL + restaurantInfoToDisplay?.cloudinaryImageId}
                alt="image"
                className="restaurantImage w-96 rounded-xl"
              />

              <div className="restaurantInfoDetails">
                <h2 className="text-xl font-semibold">
                  {restaurantInfoToDisplay?.name}
                </h2>
                <h3 className="text-lg font-semibold">
                  {restaurantInfoToDisplay?.areaName},{" "}
                  {restaurantInfoToDisplay?.city}
                </h3>
                <h3 className="text-lg font-semibold">
                  {restaurantInfoToDisplay?.avgRatingString} ⭐
                </h3>
                <h3 className="text-md">
                  {restaurantInfoToDisplay?.costForTwoMessage}
                </h3>
              </div>
            </div>
          )}

          {foodInfoToDisplay1 && (
            <h2 className="ml-64 my-4 text-xl font-semibold">
              {title1} ({foodInfoToDisplay1.length})
            </h2>
          )}

          {foodInfoToDisplay1 && (
            <div className="flex justify-center">
              <div className="foodInfo flex flex-col gap-4 justify-center">
                {foodInfoToDisplay1.map((foodInfo) => (
                  <FoodCard foodInfo={foodInfo} key={foodInfo.card.info.id} />
                ))}
              </div>
            </div>
          )}

          {foodInfoToDisplay2 && (
            <h2 className="ml-64 my-4 text-xl font-semibold">
              {title2} ({foodInfoToDisplay2.length})
            </h2>
          )}

          {foodInfoToDisplay2 && (
            <div className="flex justify-center">
              <div className="foodInfo flex flex-col gap-4">
                {foodInfoToDisplay2.map((foodInfo) => (
                  <FoodCard foodInfo={foodInfo} key={foodInfo.card.info.id} />
                ))}
              </div>
            </div>
          )}

          {foodInfoToDisplay3 && (
            <h2 className="ml-64 my-4 text-xl font-semibold">
              {title3} ({foodInfoToDisplay3.length})
            </h2>
          )}

          {foodInfoToDisplay3 && (
            <div className="flex justify-center">
              <div className="foodInfo flex flex-col gap-4">
                {foodInfoToDisplay3.map((foodInfo) => (
                  <FoodCard foodInfo={foodInfo} key={foodInfo.card.info.id} />
                ))}
              </div>
            </div>
          )}
        </div>
      )}
    </>
  );
}

export default RestaurantPage;
