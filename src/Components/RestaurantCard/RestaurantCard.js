import { useContext } from "react";
import { IMG_CDN_URL } from "../../Configs/ImageConfig";
import UserContext from "../../Utils/Shared/UserContext";

const RestaurantCard = ({
  cloudinaryImageId,
  name,
  cuisines,
  totalRatings,
}) => {
  // console.log(props)
  //* IAM GOING TO DESTRUTURE THE DATA
  // const { cloudinaryImageId, name, cuisines, lastMileTravelString } =
  //   props.restaurant.data;

  const { user } = useContext(UserContext);

  return (
    <>
      <div className="p-3 w-60 rounded-lg hover:shadow-2xl">
        {cloudinaryImageId && (
          <img
            className=" rounded-t-lg"
            src={IMG_CDN_URL + cloudinaryImageId}
            alt="image"
          />
        )}
        <h3 className="font-bold text-lg">{name}</h3>
        <h4 className="font-semibold">{cuisines.join(", ")}</h4>
        <h4>{totalRatings} + Ratings</h4>
      </div>
    </>
  );
};

export default RestaurantCard;
