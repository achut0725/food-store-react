import { createSlice } from "@reduxjs/toolkit";

const cartSlice = createSlice({
  name: "cart",
  initialState: {
    items: [],
  },
  reducers: {
    addItem: (state, action) => {
      const item = action.payload;
      const existingArrayIndex = state.items.findIndex(
        (arr) =>
          Array.isArray(arr) && arr.some((arrItem) => arrItem.id === item.id)
      );

      if (existingArrayIndex !== -1) {
        state.items[existingArrayIndex].push(item);
      } else {
        state.items.push([item]);
      }
    },
    removeItem: (state, action) => {
      const item = action.payload;
      const existingArrayIndex = state.items.findIndex(
        (arr) =>
          Array.isArray(arr) && arr.some((arrItem) => arrItem.id === item.id)
      );

      if (existingArrayIndex !== -1) {
        const existingArray = state.items[existingArrayIndex];
        const itemIndex = existingArray.findIndex(
          (arrItem) => arrItem.id === item.id
        );

        if (itemIndex !== -1) {
          existingArray.splice(itemIndex, 1);

          if (existingArray.length === 0) {
            state.items.splice(existingArrayIndex, 1);
          }
        }
      }
    },

    clearCart: (state) => {
      state.items = [];
    },
  },
});

export const { addItem, removeItem, clearCart } = cartSlice.actions;

export default cartSlice.reducer;
