import { createContext } from "react";

const LoginContext = createContext({
  loggedinStatus: {
    status: false,
  },
});

LoginContext.displayName = "LoginContext";

export default LoginContext;
