import { createContext } from "react";

const UserContext = createContext({
  user: {
    name: "Aswin Anilkumar",
    email: "aswin@gmail.com",
    phone: "7894561235",
  },
});

UserContext.displayName = "UserContext"; //* to display the name of the context in react dev tools

export default UserContext;
