import { useEffect, useState } from "react";

export function useGetDataWithId(fetchDataURL, id) {
  const [dataFromAPI, setDataFromAPI] = useState([]);

  useEffect(() => {
    getData();
  }, []);

  async function getData() {
    const response = await fetch(fetchDataURL + id);
    const data = await response.json();
    setDataFromAPI(data);
  }

  return dataFromAPI;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////*
////////////////////////////////////////////////////////////////////////////////////////////////////////////////!
////////////////////////////////////////////////////////////////////////////////////////////////////////////////*

export function useGetData(fetchDataURL) {
  const [dataFromAPI, setDataFromAPI] = useState([]);

  useEffect(() => {
    getData();
  }, []);

  async function getData() {
    const response = await fetch(fetchDataURL);
    const data = await response.json();
    setDataFromAPI(data);
  }

  return dataFromAPI;
}
