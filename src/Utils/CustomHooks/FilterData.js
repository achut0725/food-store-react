export function useFilterData(restaurants, searchText) {
  const filteredRestaurants = restaurants.filter((restaurant) =>
    restaurant?.data?.name.toLowerCase().includes(searchText.toLowerCase())
  );
  return filteredRestaurants;
}
