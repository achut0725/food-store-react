import "core-js/stable";
import "regenerator-runtime/runtime";
import React, { lazy, Suspense, useState, useContext } from "react";
import ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider, Outlet } from "react-router-dom";
import { Provider } from "react-redux";

import "./App.css";
import HeaderComponent, { Title } from "./Components/Header/HeaderComponent";
import BodyComponent from "./Components/Body/Body";
import FooterComponent from "./Components/Footer/Footer";
import ErrorPage from "./Components/ErrorPage/ErrorPage";
import RestaurantPage from "./Components/RestaurantPage/RestaurantPage";
import Profile from "./Components/Profile/Profile";
import ClassExample from "./Components/ClassExample/ClassExample";
import BodyShimmerUi from "./Components/BodyShimmerUi/BodyShimmerUi";
import UserContext from "./Utils/Shared/UserContext";
import store from "./Utils/Store/Store";
import CartPage from "./Components/CartPage/CartPage";
import Test from "../test";
import LoginContext from "./Utils/Shared/LoginContext";

const About = lazy(() => import("./Components/About/About"));
const ContactUsPage = lazy(() =>
  import("./Components/ContactUs/ContactUsPage")
);
const Instamart = lazy(() => import("./Components/Instamart/Instamart")); //* for chunking / on demand loading [ this is a promis ]

const AppLayout = () => {
  const [user, setUser] = useState({
    name: "Aswin Anilkumar",
    email: "aswinachut007@gmail.com",
    phone: "7356450725",
  });

  const [loggedinStatus, setLoggedInStatus] = useState({
    status: false,
  });

  return (
    <>
      <Provider store={store}>
        <UserContext.Provider
          value={{
            user: user,
            setUser: setUser,
          }}
        >
          <LoginContext.Provider
            value={{
              loggedinStatus: loggedinStatus,
              setLoggedInStatus: setLoggedInStatus,
            }}
          >
            <HeaderComponent />
            <Outlet />
            <FooterComponent />
          </LoginContext.Provider>
        </UserContext.Provider>
      </Provider>
    </>
  );
};

const appRouter = createBrowserRouter([
  {
    path: "/",
    element: <AppLayout />,
    errorElement: <ErrorPage />,
    children: [
      { path: "/", element: <BodyComponent /> },
      {
        path: "/about",
        element: (
          <Suspense fallback={<h3>Loading.....</h3>}>
            <About />
          </Suspense>
        ),
        children: [
          {
            path: "profile",
            element: <Profile />,
          },
        ],
      },
      {
        path: "/contactUs",
        element: (
          <Suspense fallback={<h3>Loading....</h3>}>
            <ContactUsPage />
          </Suspense>
        ),
      },
      { path: "restaurantPage/:id", element: <RestaurantPage /> },
      {
        path: "/instamart",
        element: (
          <Suspense fallback={<BodyShimmerUi />}>
            <Instamart />
          </Suspense>
        ),
      },
      {
        path: "/cartPage",
        element: <CartPage />,
      },
      {
        path: "/test",
        element: <Test />,
      },
    ],
  },
]);

const root = ReactDOM.createRoot(document.getElementById("root"));

root.render(<RouterProvider router={appRouter} />);
