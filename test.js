import React from "react";

function Test() {
  const [showContent, setShowContent] = React.useState(false);

  const handleMouseEnter = () => {
    setShowContent(true);
  };

  const handleMouseLeave = () => {
    setShowContent(false);
  };

  return (
    <div className="relative">
      <div
        className="hover:bg-gray-200"
        onMouseEnter={handleMouseEnter}
        onMouseLeave={handleMouseLeave}
      >
        Hover over me
      </div>
      {showContent && (
        <div className="absolute bg-gray-200">Content to show on hover</div>
      )}
    </div>
  );
}

export default Test;
